# box game engine

Reimplementation of game engine used in a [3D Game Engine Development Tutorial](https://www.youtube.com/playlist?list=PLEETnX-uPtBXP_B2yupUKlflXBznWIlL5) turorial series in a C++ (and Java) language.


## structure

**cpp**: c++ engine implementation

**doc**: project documentation (if any) and `readme.md` stuff

**java**: java engine implementation


## build

...


# wolf3d (an android sample)

[![wolf3d sample (android)](doc/wolf3d_sample_android_video_prev.png)](https://www.youtube.com/watch?v=C5z-mFXPiSA)
