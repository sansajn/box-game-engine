#version 330

in vec2 texCoord0;
uniform vec3 color;
uniform sampler2D sampler;
out vec4 fcolor;

void main()
{
	vec4 textureColor = texture2D(sampler, texCoord0);
	if (textureColor == 0)
		fcolor = vec4(color, 1);
	else
		fcolor = textureColor * vec4(color, 1.0);
}
