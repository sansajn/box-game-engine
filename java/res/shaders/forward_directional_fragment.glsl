#version 330
#include "lighting_fragment.glsl"

in vec2 texCoord0;
in vec3 normal0;
in vec3 worldPos0;

uniform sampler2D diffuse;
uniform DirectionalLight directionalLight;

out vec4 fcolor;


void main()
{
	vec4 dirLightColor = calcDirectionalLight(directionalLight, normalize(normal0), worldPos0);
	fcolor = texture(diffuse, texCoord0) * dirLightColor;
}
