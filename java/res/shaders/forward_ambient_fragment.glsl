#version 330

in vec2 texCoord0;

uniform vec3 ambientIntensity;
uniform sampler2D diffuse;

out vec4 fcolor;

void main()
{
	fcolor = texture(diffuse, texCoord0) * vec4(ambientIntensity, 1);
}
