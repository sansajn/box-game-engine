#version 330
#include "lighting_fragment.glsl"

in vec2 texCoord0;
in vec3 normal0;
in vec3 worldPos0;

uniform sampler2D diffuse;
uniform PointLight pointLight;

out vec4 fcolor;

void main()
{	
	vec4 pointLightColor = calcPointLight(pointLight, normalize(normal0), worldPos0);
	fcolor = texture(diffuse, texCoord0) * pointLightColor;
}
