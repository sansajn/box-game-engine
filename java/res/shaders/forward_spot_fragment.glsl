#version 330
#include "lighting_fragment.glsl"

in vec2 texCoord0;
in vec3 normal0;
in vec3 worldPos0;

uniform sampler2D diffuse;
uniform SpotLight spotLight;

out vec4 fcolor;


void main()
{
	vec4 spotLightColor = calcSpotLight(spotLight, normalize(normal0), worldPos0);
	fcolor = texture(diffuse, texCoord0) * spotLightColor;
}
 
