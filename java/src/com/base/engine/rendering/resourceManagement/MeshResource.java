package com.base.engine.rendering.resourceManagement;

import static org.lwjgl.opengl.GL15.*;

public class MeshResource {
	public MeshResource(int size) {
		vbo = glGenBuffers();
		ibo = glGenBuffers();
		this.size = size;
		this.refCount = 1;
	}

	@Override
	protected void finalize() {
		glDeleteBuffers(vbo);
		glDeleteBuffers(ibo);
	}

	public int getVbo() {return vbo;}
	public int getIbo() {return ibo;}
	public int getSize() {return size;}

	public void addReference() {refCount += 1;}
	public boolean removeReference() {return refCount-- == 0;}

	private int vbo;
	private int ibo;
	private int size;
	private int refCount;
}
