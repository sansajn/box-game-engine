package com.base.engine.rendering.resourceManagement;

import static org.lwjgl.opengl.GL11.*;

public class TextureResource {
	public TextureResource() {
		this.id = glGenTextures();
		this.refCount = 1;
	}

	@Override
	protected void finalize() {glDeleteTextures(id);}

	public int getId() {return id;}

	public void addReference() {refCount += 1;}
	public boolean removeReference() {return refCount-- == 0;}

	private int id;
	private int refCount;
}
