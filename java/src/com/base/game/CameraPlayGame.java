package com.base.game;

import com.base.engine.components.*;
import com.base.engine.core.*;
import com.base.engine.rendering.*;

public class CameraPlayGame extends Game {
	static Vector3f XAXIS = new Vector3f(1,0,0);
	static Vector3f YAXIS = new Vector3f(0,1,0);
	static Vector3f ZAXIS = new Vector3f(0,0,1);

	@Override public void init() {
		// vytvor scenegraph do ktoreho umiestni kameru a nieco na co sa mozem pozerat
		cameraObject = new GameObject();
		cameraObject.getTransform().setPos(new Vector3f(0,5,0));
		cameraObject.addComponent(new FreeMove()).addComponent(new FreeLook()).addComponent(new Camera((float) Math.toRadians(70), Window.getWidth() / Window.getHeight(), 0.01f, 1000));
		addObject(cameraObject);

		GameObject planeObject = createPlane();
		addObject(planeObject);

		monkeyObject = createMonkey();
//		monkeyObject.getTransform().setPos(new Vector3f(0,5,5));
//		monkeyObject.getTransform().setRot(new Quaternion(new Vector3f(0,1,0), (float)Math.toRadians(180)));
//		monkeyObject.addComponent(new LookAtComponent());
		addObject(monkeyObject);

//		GameObject lightObject = createLight();
//		lightObject.getTransform().setPos(new Vector3f(0,0,0));
//		lightObject.getTransform().setRot(new Quaternion(XAXIS, (float)Math.toRadians(-90)));
//		addObject(lightObject);
	}

	GameObject createLight() {
		GameObject light = new GameObject();
		light.addComponent(new DirectionalLight(new Vector3f(1, 1, 1), 0.5f));

		return light;
	}

	@Override public void update(float delta) {
		// opicu chcem vydiet stale pred sebou
		Vector3f cameraPos = cameraObject.getTransform().getPos();
		Vector3f monkeyPos = cameraPos.add(cameraObject.getTransform().getRot().getForward().mul(5));
		monkeyObject.getTransform().setPos(monkeyPos);

		// natoc opicu, tak aby pozerala stale na mna
		Vector3f directionToMonkey = monkeyPos.sub(cameraPos);
		double angle = Math.atan2(directionToMonkey.getZ(), directionToMonkey.getX());
		monkeyObject.getTransform().setRot(new Quaternion(YAXIS, (float)(Math.toRadians(90) - angle + Math.toRadians(180))));
		System.out.println("angle:" + Math.toDegrees(angle));

		super.update(delta);
	}

	GameObject createMonkey() {
		Mesh mesh = new Mesh("monkey3.obj");

		Material material = new Material();
		material.addTexture("diffuse", new Texture("checker.png"));

		GameObject object = new GameObject();
		object.addComponent(new MeshRenderer(mesh, material));

		return object;
	}

	GameObject createPlane() {
		float fieldDepth = 10f;
		float fieldWidth = 10f;
		Vertex[] vertices = new Vertex[] {new Vertex(new Vector3f(-fieldWidth, 0, -fieldDepth), new Vector2f(0, 0)),
				new Vertex(new Vector3f(-fieldWidth, 0, fieldDepth*3), new Vector2f(0, 1)),
				new Vertex(new Vector3f(fieldWidth*3, 0, -fieldDepth), new Vector2f(1, 0)),
				new Vertex(new Vector3f(fieldWidth*3, 0, fieldDepth*3), new Vector2f(1, 1))};

		int[] indices = new int[] {0, 1, 2,
				2, 1, 3};

		Mesh mesh = new Mesh(vertices, indices, true);

		Material material = new Material();
		material.addTexture("diffuse", new Texture("checker.png"));
		material.addFloat("specularIntensity", 1);
		material.addFloat("specularPower", 8);

		GameObject object = new GameObject();
		object.addComponent(new MeshRenderer(mesh, material));

		return object;
	}

	private GameObject monkeyObject;
	private GameObject cameraObject;
}
