package com.base.game;

import com.base.engine.core.CoreEngine;

public class CameraPlayMain {
	static public void main(String[] args) {
		CoreEngine engine = new CoreEngine(800, 600, 60, new CameraPlayGame());
		engine.createWindow("Camera play");
		engine.start();
	}
}
